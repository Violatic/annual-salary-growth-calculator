"""
This calculator was designed to run on python 2.7
If it is in python 3 it will fail due to the change in print
"""

import sys


def simple_calc(start, end, years):
    """
    simple_calc calculates the percentage change in salary per year,
    takes that ratio to the power 1 over time then changes that to a percentage
    """
    # Floats are used since in python2 division is weird without this
    percentage_change = float(end) / float(start)
    annual_growth = percentage_change**(1/float(years))
    annual_rate_of_growth = (annual_growth - 1)*100
    return annual_rate_of_growth


def advanced_calc(start, end, years):
    """
    advanced_calc calculates the percentage change in salary per year,
    takes that ratio to the power 1 over time then changes that to a percentage
    it outputs all 3 outputs as a tuple rather than a single value.
    """
# TODO: remove simple / advanced redundancy - very similar
    percentage_change = float(end) / float(start)
    annual_growth = percentage_change**(1/float(years))
    annual_rate_of_growth = (annual_growth - 1)*100
    return (percentage_change, annual_growth, annual_rate_of_growth)


# TODO: FULL CALC - requires table output for number of years, total earned
# and graphical output

# This is required so that when the code exists a traceback warning isn't
# thrown to the user
sys.trackbacklimit = 0

starting_salary = input("What is your starting salary?: ")
final_salary = input("What is your goal final salary?: ")
years_to_final = input("""How many years will pass between your starting
                          salary and final salary?: """)
complication_level = raw_input(""""Do you want simple, advanced,
                                or full analysis?: """)

if complication_level == "simple":
    output = simple_calc(starting_salary, final_salary, years_to_final)
    print "Your annual required growth is " + str(round(output, 2)) + "%"
    sys.exit(1)
elif complication_level == "advanced":
    output = advanced_calc(starting_salary, final_salary, years_to_final)
    print "The percentage change in salary is " \
          + str(round(output[0], 2)) + "%"
    print "The annual growth in salary is " + str(round(output[1], 2))
    print "The annual growth percentage in salary is " \
          + str(round(output[2], 2)) + "%"
